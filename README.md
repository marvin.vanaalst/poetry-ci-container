# README

```
docker login
VERSION=0.3
docker build  --tag marvinvanaalst/poetry-ci:${VERSION} -f Base.Dockerfile .
docker push marvinvanaalst/poetry-ci:${VERSION}

VERSION=0.2
docker build  --tag marvinvanaalst/poetry-ci-extended:${VERSION} -f Extended.Dockerfile .
docker push marvinvanaalst/poetry-ci-extended:${VERSION}
```
