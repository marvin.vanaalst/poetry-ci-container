FROM marvinvanaalst/poetry-ci:0.3

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential cmake python-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*
