FROM condaforge/mambaforge

ENV PATH="${PATH}:/root/.local/bin:$PATH"

RUN apt-get update && apt-get install -y --no-install-recommends curl \
    && curl -sSL https://install.python-poetry.org | POETRY_PREVIEW=1 python3 - \
    # apt clean up
    && apt-get clean && rm -rf /var/lib/apt/lists/* 
